# Sistema Gerenciador de Tarefas - SGT

O SGT é um projeto que visa facilitar o cotidiano das pessoas, desenvolvido na disciplina de Aplicações Distribuídas sob a orientação do professor [Marcelo Akira Inuzuka](http://lattes.cnpq.br/0701953063536783)

### Manual do usuário

Para ter acesso ao SGT é necessário baixar e instalar os seguintes softwares:

 (Clique no nome para realizar os downloads)

* [Nodejs](https://nodejs.org/en/)
* [Insomnia](https://insomnia.rest/download/)
* [MongoDB](https://www.mongodb.com/try/download/community)
* [Visual Studio Code](https://code.visualstudio.com/download)



Abra VSCode, dentro dele um novo terminal e execute os comandos a seguir (linha a linha, um por vez) para baixar o repositório do SGT e executá-lo.

```
git clone https://gitlab.com/sd-si-2020-1/g2.git
cd .\GerenciadordeTarefa\
npm init -y
npm install express
npm install date-fns
npm install mongoose
node .\src\index.js 
```

Para acessar a documentação Swagger [clique aqui!](https://gitlab.com/sd-si-2020-1/g2/-/blob/teste/Documentos/swagger.yaml)

Para acessar os testes de rota utilizados [clique aqui!](https://gitlab.com/sd-si-2020-1/g2/-/tree/teste/Documentos/Insomnia) 


#### Foi criada uma classe taskController contendo todos os métodos responsáveis pelo gerenciamento e execução das funcionalidades do aplicativo. 

#### Essa classe possui os seguintes métodos:

- Create: Responsável pela criação da tarefa.

        async create(req, res){
            const task = new TaskModel(req.body);
            await task
                .save()
                .then(response => {
                    return res.status(200).json(response);
                    })
                .catch(error => {
                        return res.status(500).json(error);
                    });
        }


- Update: Responsável por fazer alterações na tarefa.

        async update(req, res){
            await TaskModel.findByIdAndUpdate({'_id': req.params.id}, req.body, {new: true})
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(error => {
                return res.status(500).json(error);
            });
        }


- All: Responsável por apresentar todas tarefas de um usuário.

        async all(req, res){
            await TaskModel.find({macaddress: {'$in': req.body.macaddress}})
            .sort('when')
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(error => {
                return res.status(500).json(error);
            });
            
        }   

- Show: Responsável por buscar e apresentar uma tarefa específica.

        async show(req, res){
            await TaskModel.findById(req.params.id)
            .then(response => {
                if(response)
                    return res.status(200).json(response);
                else
                return res.status(404).json({error: 'tarefa não encontrada'});

            })
            .catch(error => {
                return res.status(500).json(error);
            })
        }


	
- Delete: Responsável por deletar uma tarefa cadastrada.

        async delete(req, res){
            await taskmodel.deleteOne({'_id': req.params.id})
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(error => {
                return res.status(500).json(error);
            });
        }

- Done: Responsável por marcar atividade como finalizada.

        async done(req, res){
            await TaskModel.findByIdAndUpdate(
                {'_id': req.params.id},
                {'done': req.params.done},
                {new: true})
                .then(response => {
                    return res.status(200).json(response);
                })
                .catch(error => {
                    return res.status(500).json(error);
                });
        }

- Late: Responsável por filtrar atividades atrasadas.

        async late(req, res){
            await TaskModel.find({
                'when': {'$lt': current},
                'macaddress': {'$in': req.body.macaddress}
            })
            .sort('when')
            .then(response => {
                return res.status(200).json(response);
            })
            .catch(error => {
                return res.status(500).json(error);
            });
        }

- Today: Responsável por filtrar atividades do dia.

        async today(req, res){
            await TaskModel
                    .find({
                    'macaddress': {'$in': req.params.macaddress},
                    'when': {'$gte': startOfDay(current), '$lt': endOfDay(current)}
                    })
                    .sort('when')
                    .then(response => {
                        return res.status(200).json(response);
                    })
                    .catch(error => {
                        return res.status(500).json(error);
                    });
        }

- Week: Responsável por filtrar atividades da semana.

        async week(req, res){
            await TaskModel
                    .find({
                    'macaddress': {'$in': req.params.macaddress},
                    'when': {'$gte': startOfDay(current), '$lt': endOfDay(current)}
                    })
                    .sort('when')
                    .then(response => {
                        return res.status(200).json(response);
                    })
                    .catch(error => {
                        return res.status(500).json(error);
                    });
        }

- Month: Responsável por filtrar atividades do mês.

        async month(req, res){
            await TaskModel
                    .find({
                    'macaddress': {'$in': req.params.macaddress},
                    'when': {'$gte': startOfMonth(current), '$lt': endOfMonth(current)}
                    })
                    .sort('when')
                    .then(response => {
                        return res.status(200).json(response);
                    })
                    .catch(error => {
                        return res.status(500).json(error);
                    });
        }



- Year: Responsável por filtrar atividades do ano.

        async year(req, res){
            await TaskModel
                    .find({
                    'macaddress': {'$in': req.params.macaddress},
                    'when': {'$gte': startOfYear(current), '$lt': endOfYear(current)}
                    })
                    .sort('when')
                    .then(response => {
                        return res.status(200).json(response);
                    })
                    .catch(error => {
                        return res.status(500).json(error);
                    });
        }

#### Classe responsável pela conexão com banco de dados MongoDB.

        const mongoose = require ('mongoose');

        const url = 'mongodb://localhost:27017/todo';

        mongoose.connect(url, {useNewUrlParser: true});

        module.exports = mongoose;  


#### Classe responsável por criar modelo banco de dados.

        const mongoose = require('../config/bancodados');

        const Schema = mongoose.Schema;

        const taskSchema = new Schema({
            macaddress: {type: String, required: true},
            type: {type: Number, required:true},
            title: {type: String, required: true}, 
            description: {type: String, required: true},
            when: {type: Date, required: true},
            done: {type: Boolean, default: false},
            created: {type: Date, default: Date.now }
        });

        module.exports = mongoose.model('task', taskSchema);

#### Classe responsável por validação de cadastro de tarefa.


        const taskModel = require('../model/taskmodel');
        const {isPast} = require('date-fns');


        const taskValidation = async (req, res, next) => {
            const {macaddress, type, title, description, when} = req.body;

            if(!macaddress)
                return res.status(400).json({error: 'macaddress é obrigatoria'});
            else if(!type)
            return res.status(400).json({error: 'tipo obrigatorio'});
            else if(!title)
            return res.status(400).json({error: 'titulo obrigatorio'});
            else if(!description)
            return res.status(400).json({error: 'descrição obrigatorio'});
            else if(!when)
            return res.status(400).json({error: 'data obrigatorio'});
            else if(isPast(new Date(when)))
            return res.status(400).json({error: 'data no futuro'});
            else{
                let exists;
                if(req.params.id){
                    exists = await taskModel
                    .findOne({
                    '_id': {'$ne': req.params.id},
                    'when': {'$eq': new Date(when)},
                    'macaddress': {'$in': macaddress}
                    });
                }else{
                        exists = await taskModel.
                        findOne({'when': {'$eq': new Date(when)},
                            'macaddress': {'$in': macaddress}
                        });
                    }
                if (exists){
                    return res.status(400).json({error: 'Já existe'});
                }
                next();
                }

        }

        module.exports = taskValidation;
