# Relatório Final


#### Matheus Henrique de Brito

##### [Vídeo da apresentação](https://www.youtube.com/watch?v=3PaQhL25MYc)
- [x] 14/09 a 18/09: Entendimento das ferramentas e estudo sobre JS, NodeJS, definição do problema e definição das ferramentas que seriam utilizadas como MongoDB e Insomnia

- [x] 25/09 a 15/11: Ínicio e fim da codificação do projeto.

- Projeto iniciado, colocando o servidor online e escutando na porta 3000 utilizando o express.js. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/8a9f053dd8fada7f73c80f27f83d8bfbbdda3a96)

- Criação de algumas rotas, o schema, a integração com o banco de dados mongoDB e as regras de validação contidas no controller. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/d98ee5e77d62b39f16f5f1d0af17ab20ec24cb40)

- Criação final de todas as rotas para filtrar por mês, ano e dia. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/9e374c4a5a353a83f2f26ae82c43185180cfbe4f)

- [x] 21/10 a 10/11: Inicio e término da documentação de testes e Documentação Swagger:
- Inicio da documentação de testes, com todos os testes realizados e documentados com entradas, saidas, datas na planilha. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/c5cca2c142d37d0386e108eab3bc26c22fd50150)
- Finalização dos testes realizados e organização do plano de testes. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/28c9e2492dc2e8d8688f63a44eb6479dd55287b6)
- Criação da documentação swagger a partir do editor swagger e disponibilizada em markdown. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/e1c605bd67cf660090fb2ec1b7db23732c9390b3)


#### Vinicius Barreto Costa

- Feito entendimento do código do projeto.

- Iniciada confecção da documentação. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/5c418342fe4151a5fb5e0fdc43765dc0d9a77a15)

- Feito primeira parte do Relatório Técnico. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/2e2b60553dde6aff4c4d5f138bfab8f52abdb3e8)

- Entregue a segunda parte do Relatório Técnico. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/4dd99ca871817f3e019dbbeeb4f86162af4fe688)

- Concluído Relatório Técnico. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/3a57f02a4de1285936d457dd446d538e483fb5c7)

- Vídeo de apresentação individual. [Video YouTube](https://youtu.be/sEuvzj0eZqI)

- Vídeo de apresentação individual. [Video YouTube](https://youtu.be/-1rk3ymaLwQ)

#### Victor Vidal Vaz

* Documentação das ferramentas utilizadas no SGT. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/43cc386ceed025ac9fad07a61b34ec4d1769d151)

* Documentação da Arquitetura do SGT. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/fa71017eb16e8f378c7e45e1daa086561c4f2f56)

* Inserção do Design Pattern - MVC . [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/43cc386ceed025ac9fad07a61b34ec4d1769d151)

* Documentação do Diagrama de Atividade. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/6bac2e3e8039ba5957d2c323212100ae5c269e92)

* Documentação dos Casos de Uso (alterados na data de hoje). [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/db7f51bb5cdc6a1edf44a98fffe81279768f412d)

* Documentação do Diagrama de Sequência (alterados na data de hoje). [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/582d28224396b21ba7499813a48029103715d7e8)

* Documentação dos Requisitos. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/c6a251335e4a171fcfba026b88d52252005508f3)

* Manual do Usuário. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/aa30f0cd8a7517b6325e6a2f5473e2c881c13a1d)

* Criação de algumas Issues. [link](https://gitlab.com/sd-si-2020-1/g2/-/issues?scope=all&utf8=✓&state=closed)

* Vídeo de apresentação individual. [link](https://youtu.be/2tm4mFgwbuo) 


