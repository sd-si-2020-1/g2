/**
 *  @swagger
 *   components:
 *     schemas:
 *       Book:
 *         type: object
 *         required:
 *           - title
 *           - author
 *           - finished
 *         properties:
 *           id:
 *             type: integer
 *             description: The auto-generated id of the book.
 *           title:
 *             type: string
 *             description: The title of your book.
 *           author:
 *             type: string
 *             description: Who wrote the book?
 *           finished:
 *             type: boolean
 *             description: Have you finished reading it?
 *           createdAt:
 *             type: string
 *             format: date
 *             description: The date of the record creation.
 *         example:
 *            title: The Pragmatic Programmer
 *            author: Andy Hunt / Dave Thomas
 *            finished: true
*/
const express = require('express');
const router = express.Router();

const TaskControl = require('../controller/taskcontrol');
const TaskValidation = require('../middleware/taskvalidacao');
const macaddressvalidacao = require('../middleware/macaddressvalidacao');

router.post('/', TaskValidation, TaskControl.create);
router.put('/:id', TaskValidation, TaskControl.update);
router.get('/filter/all/:macaddress', TaskControl.all);
router.get('/:id', TaskControl.show);
router.delete('/:id', TaskControl.delete);
router.put('/:id/:done', TaskControl.done);
router.get('/filter/late/:macaddress', TaskControl.late); 
router.get('/filter/today/:macaddress', TaskControl.today);
router.get('/filter/week/:macaddress', TaskControl.week);
router.get('/filter/month/:macaddress', TaskControl.month);
router.get('/filter/year/:macaddress', TaskControl.year);

module.exports = router;
