const express = require ('express');
const server = express();
server.use(express.json());
bodyParser = require("body-parser"),
swaggerJsdoc = require("swagger-jsdoc"),
swaggerUi = require("swagger-ui-express");
const TaskRoutes = require('./routes/taskroutes');

server.use('/task', TaskRoutes);

server.listen(3000, () => {
    console.log('ONLINE');
});


