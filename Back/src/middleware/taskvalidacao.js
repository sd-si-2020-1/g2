const taskModel = require('../model/taskmodel');
const {isPast} = require('date-fns');


const taskValidation = async (req, res, next) => {
    const {macaddress, type, title, description, when} = req.body;

    if(!macaddress)
        return res.status(400).json({error: 'macaddress é obrigatoria'});
     else if(!type)
      return res.status(400).json({error: 'tipo obrigatorio'});
     else if(!title)
       return res.status(400).json({error: 'titulo obrigatorio'});
     else if(!description)
      return res.status(400).json({error: 'descrição obrigatorio'});
     else if(!when)
       return res.status(400).json({error: 'data obrigatorio'});
     else if(isPast(new Date(when)))
       return res.status(400).json({error: 'data no futuro'});
       else{
           let exists;
           if(req.params.id){
             exists = await taskModel
             .findOne({
              '_id': {'$ne': req.params.id},
              'when': {'$eq': new Date(when)},
             'macaddress': {'$in': macaddress}
            });
           }else{
                exists = await taskModel.
                   findOne({'when': {'$eq': new Date(when)},
                    'macaddress': {'$in': macaddress}
                });
            }
        if (exists){
            return res.status(400).json({error: 'Já existe'});
        }
        next();
        }

}

module.exports = taskValidation;
