# Requisitos Funcionais

O propósito deste documento é listar os requisitos funcionais do sistema, colhidos como história do usuário. Através dos requisitos levantados abaixo é que são gerados os Casos de Uso para o SGT.



| Requisito Funcional | Descrição                                                    | Código |
| ------------------- | ------------------------------------------------------------ | ------ |
| Criar uma tarefa    | Como usuário, quero conseguir criar uma tarefa para que possa acompanhar e executar. | RF1    |
| Excluir uma tarefa  | Como usuário, gostaria de excluir uma tarefa no momento em que me for conveniente. | RF2    |
| Editar uma tarefa   | Como usuário, gostaria de poder alterar uma tarefa previamente criada para reparar erros de descrição, o mesmo alterar data e hora da execução da mesma. | RF3    |
| Concluir uma tarefa | Como usuário, gostaria de poder marcar sempre que uma tarefa previamente criada estiver concluída, facilitando a visualização das tarefas que tenho ainda que cumprir e de quais já foram cumpridas. | RF4    |
| Buscar tarefas      | Como usuário, quero poder fazer uma busca em minhas tarefas para me organizar melhor a maneira de execução. | RF5    |
| Pesquisar atrasadas | Como usuário gostaria de ser notificado e poder visualizar sempre que houver uma tarefa vencida para executar | RF6    |

