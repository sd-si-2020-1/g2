# Insomnia

O insomnia é uma ferramenta para teste de rotas CRUD (GET, PUT, POST, DELETE). Para ter acesso aos testes de rotas do SGT baixe o arquivo InsomniaTestes contido neste diretório e faça o [download da ferramenta](https://insomnia.rest/download/), em seguida execute os seguintes passos.



##### Passo 1

![img1](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/Insomnia/Tela1Insomina.png) 



##### Passo 2

![img2](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/Insomnia/Tela2Insomina.png) 

##### Passo 3

![img3](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/Insomnia/Tela3Insomina.png) 

Feito isso, basta submeter os testes para obter as respostas e verificar o funcionamento de cada funcionalidade.