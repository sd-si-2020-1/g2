# Plano de Testes


Neste Plano, é descrito os principais fatores que serão o guia para a realização dos testes da API - Gerenciador de Tarefas.
Esta API realiza funções de criação, exclusão, atualização e filtragens pelos diversos fatores temporais como ano, mês e dia das tarefas cadastradas pelos usuários do sistema.

## 1. Itens de Testes:

**Serão itens de teste deste respectivo plano de testes:**

    
- As rotas geradas pelo código descritas a seguir: 
    `router.post('/', TaskValidation, TaskControl.create);`
    `router.put('/:id', TaskValidation, TaskControl.update);`
    `router.get('/filter/all:macaddress', TaskControl.all);`
    `router.get('/:id', TaskControl.show);`
    `router.delete('/:id', TaskControl.delete);`
    `router.put('/:id/:done', TaskControl.done);`
    `router.get('/filter/late/:macaddress', TaskControl.late);` 
    `router.get('/filter/today/:macaddress', TaskControl.today);`
    `router.get('/filter/week/:macaddress', TaskControl.week);`
    `router.get('/filter/month/:macaddress', TaskControl.month);`
    `router.get('/filter/year/:macaddress', TaskControl.year);`

-     A validação feita para impedir objetos JSON incompletos, realizada por TaskValidation:

    `const taskValidation = async (req, res, next)`

-     Cada uma das funções contidas no taskController que são os parâmetros para as chamadas das rotas

-     Por fim, será testada a integração e persistência com o MongoDB:

    `const mongoose = require ('mongoose');`
    `const url = 'mongodb://localhost:27017/todo';`
    `mongoose.connect(url, {useNewUrlParser: true});`
    `module.exports = mongoose;`



## 2. Os tipos de teste serão Dinâmicos e realizados com a ferramenta Insomnia. 

## 3. Resultados dos Testes:  [PlanilhaTestes](https://docs.google.com/spreadsheets/d/1G2oO6zu6FNr3gM5SK_j0wMmPqSEgNaD3EuO4bPC3Mf8/edit?usp=sharing)


