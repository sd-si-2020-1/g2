# Diagramas de Sequência - Fluxo Normal



## Caso de uso 1

![DUC1](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/Diagramas%20de%20Sequencia/DUC1.png)

## Caso de uso 2

![DUC2](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/Diagramas%20de%20Sequencia/DUC2.png)

## Caso de uso 3

![DUC3](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/Diagramas%20de%20Sequencia/DUC3.png)

## Caso de uso 4

![DUC4](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/Diagramas%20de%20Sequencia/DUC4.png)

## Caso de uso 5

![DUC5](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/Diagramas%20de%20Sequencia/DUC5.png)

## Caso de uso 6

![DUC6](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/Diagramas%20de%20Sequencia/DUC6.png)