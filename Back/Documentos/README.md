------

# SGT - Sistema Gerenciador de Tarefas

### Documento de Arquitetura de Software

#### Versão 1.0



| Data       | Versão | Descrição              | Autor            |
| ---------- | ------ | ---------------------- | ---------------- |
| 12/10/2020 | 1.0    | Início da documentação | Vinícius Barreto |
| 14/10/2020 | 1.0    | Ferramentas utilizadas | Victor Vidal     |
| 22/10/2020 | 1.0    | Arquitetura            | Victor Vidal     |
| 23/10/2020 | 1.0    | MVC                    | Victor Vidal     |
| 29/10/2020 | 1.0    | Diagrama de Atividade  | Victor Vidal     |
| 04/11/2020 | 1.0    | Casos de Uso           | Victor Vidal     |
| 12/11/2020 | 1.0    | Diagrama de Sequência  | Victor Vidal     |

### **Introdução**

Será desenvolvido um software para gerenciamento de tarefas com  controle, permitindo ao usuário ter os registros feitos via aparelho celular por aplicativo mobile, verificar tarefas diárias, semanais e mensais com a possibilidade de alteração de seus status, edição de tarefa, exclusão e filtro de busca. Este documento oferece uma visão geral arquitetural abrangente do sistema, usando diversas visões arquiteturais para representar diferentes aspectos do sistema. O objetivo deste documento é capturar e comunicar as decisões arquiteturais significativas que foram tomadas.

#### **Arquitetura**

![Arquitetura](Arquitetura.png)

A arquitetura do Sistema de Gerenciamento de Tarefas é dividido em três camadas, sendo elas:

* Interface - Parte FrontEnd da implementação, será realizada em React para implementar a interface gráfica de um aplicativo de celular, que por sua vez consumirá a API REST.
* Aplicação - BackEnd do projeto, aqui todo processamento, validação e comunicação entre a interface e o banco de dados será feito.
* Memória - Responsável por fazer com que os dados persistam e possam ser consultados portanto sempre que necessário.

#### Model **View Controller - MVC**

Para o desenvolvimento do sistema gerenciador de tarefas foi utilizado o [MVC](https://www.devmedia.com.br/introducao-ao-padrao-mvc/29308), que é um padrão de arquitetura que trás uma definição mais clara  em suas funções, e por sua vez muito bem dividida em três camadas.

![MVC](MVC.png)

* Model - Camada responsável pela manipulação dos dados (leitura e escrita) e também pela validação dos mesmos quando solicitado pela camada Controller.
* Controller - Esta camada recebe todas as requisições enviadas pela camada View (que por sua vez são feitas pelo usuário) e as processa, solicitado consulta ou solicitações de escrita em banco (comunicando diretamente com a camada Model), ou enviando dados recuperados do banco de dados a camada View para que seja exibido ao usuário.
* View - Esta camada recebe a interação do usuário e as envia para camada Controller processar, solicitar os dados a camada Model, que por sua vez repassa de volta até a camada View novamente para que seja exibida ao usuário.

#### **Tecnologias Utilizadas**

* [Node.js](https://nodejs.org/pt-br/about/) - Plataforma construída sobre o motor JavaScript do Google Chrome para construir aplicações de rede rápidas e escaláveis;
* [MongoDB](https://www.mongodb.com) - Banco de dados NoSQL baseado em documentos (BSON);
* [Mongoose](https://mongoosejs.com/docs/guide.html) - Modelo de dados de objeto (ODM) baseado em programação orientada a objetos;
* [JSON](https://www.json.org/json-pt.html) - Formato de representação de dados baseado na linguagem de programação JavaScript, daí o nome JavaScript Object Notation;
* [Insomnia](https://support.insomnia.rest) - Software para teste de rotas de API Rest, levando em conta que os navegadores realizam somente requisições (GET), é necessário um software para testar outras rotas (PUT, POST, DELETE);
* [Express](https://expressjs.com) - Framework responsável por deixar o servidor online;
* [Nodemon](https://github.com/remy/nodemon#nodemon) - Ferramenta de desenvolvimento que torna possível, sempre que alguma informação no arquivo de execução for alterada, o servidor ser automaticamente reiniciado, evitando assim a necessidade de inicializar manualmente. A ferramenta pode ser inserida como script dentro de  package.json e ser chamada através da mensagem definida, como "start" por exemplo;
* [Date-fns](https://date-fns.org/docs/Getting-Started) - Framework utilizado para realizar a validação de data e hora.


#### **Casos de Uso**

O Sistema Gerenciador de Tarefas Conta com os seguintes casos de uso:

* [UC1 - Criar Tarefa](https://gitlab.com/sd-si-2020-1/g2/-/blob/teste/Documentos/CasosDeUso/UC1-CRIAR_TAREFA.png)

* [UC2 - Remover Tarefa](https://gitlab.com/sd-si-2020-1/g2/-/blob/teste/Documentos/CasosDeUso/UC2-REMOVER_TAREFA.png)

* [UC3 - Atualizar Tarefa](https://gitlab.com/sd-si-2020-1/g2/-/blob/teste/Documentos/CasosDeUso/UC3-ATUALIZAR_TAREFA.png)

* [UC4 - Alterar Status](https://gitlab.com/sd-si-2020-1/g2/-/blob/teste/Documentos/CasosDeUso/UC4-ALTERAR_STATUS.png)

* [UC5 - Filtrar Tarefas](https://gitlab.com/sd-si-2020-1/g2/-/blob/teste/Documentos/CasosDeUso/UC5-FILTRAR_TAREFAS.png)

* [UC6 - Exibir Atrasadas](https://gitlab.com/sd-si-2020-1/g2/-/blob/teste/Documentos/CasosDeUso/UC6-EXIBIR_ATRASADAS.png)


#### **Diagrama de Atividades**


![Diagrama De Atividade](DiagramaAtividade.png)

O diagrama de atividade mostra de forma geral o que os casos de uso podem executar no sistema em seu fluxo normal ou no fluxo alternativo de forma simplificada em uma única imagem, tornando mais amigável a compreensão do que o Sistema Gerenciador de Tarefas pode executar.  Vale lembrar que "Atualiza Status" e "Atualiza Tarefa" funcionam a mesma maneira, caso a tarefa exista ela é atualizada (seja por suas informações ou por seu status), caso não ela exibe o erro. 

#### **Diagrama de Sequência**

O diagrama de sequência mostra como o determinado caso de uso se comporta desde a interação do usuário com a interface gráfica até a resposta que o usuário recebe da API.

![Diagrama de Sequencia - Caso de Uso 1](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/Diagramas%20de%20Sequencia/DUC1.png)


Os outros diagramas de sequencia podem ser visitados clicando [aqui](https://gitlab.com/sd-si-2020-1/g2/-/tree/teste/Documentos/Diagramas%20de%20Sequencia)



