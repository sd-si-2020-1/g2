# Casos de uso

Um caso de uso é uma funcionalidade do sistema que é executada por um usuário. Casos de uso são definidos a partir de um ou mais [requisitos](https://gitlab.com/sd-si-2020-1/g2/-/tree/teste/Documentos/Requisitos) como podem ser vistos na tabela a seguir, que descreve e cita os casos de uso do SGT e os requisitos que o compõe.



| CASO DE USO |       NOME       | DESCRIÇÃO                                                    | REQUISITO FUNCIONAL |
| :---------: | :--------------: | :----------------------------------------------------------- | :-----------------: |
|     UC1     |   Criar Tarefa   | Um usuário cria uma nova tarefa no SGT inserindo tipo, título, descrição, data e hora. |         RF1         |
|     UC2     |  Remover Tarefa  | Um usuário pode remover uma tarefa previamente criada, retirando-a de sua lista de atividades. |         RF2         |
|     UC3     | Atualizar Tarefa | Um usuário pode editar uma tarefa alterando seu tipo, título, descrição, data ou hora a maneira que lhe for conveniente para que se adapte a suas novas necessidades. |         RF3         |
|     UC4     |  Alterar Status  | Um usuário pode alterar o status de uma tarefa para concluída, retirando-a de seu campo de visão de tarefas a serem feitas. |         RF4         |
|     UC5     | Filtrar Tarefas  | Um usuário pode filtrar as tarefas de modo a facilitar sua visualização e planejamento do dia, semana, mês ou ano. |         RF5         |
|     UC6     | Exibir Atrasadas | O usuário pode exibir as tarefas em atraso de modo a alterar a data das mesmas (reagendar), ou dar aquela atividade prioridade para recuperar o atraso. |         RF6         |



## UC1 - Cadastrar Tarefa

![UC1](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/CasosDeUso/UC1-CRIAR_TAREFA.png)

## UC2 - Remover Tarefa

![UC2](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/CasosDeUso/UC2-REMOVER_TAREFA.png)

## UC3 - Atualizar Tarefa

![UC3](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/CasosDeUso/UC3-ATUALIZAR_TAREFA.png)

## UC4 - Alterar Status

![UC4](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/CasosDeUso/UC4-ALTERAR_STATUS.png)

## UC5 - Filtrar Tarefas

![UC5](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/CasosDeUso/UC5-FILTRAR_TAREFAS.png) 

## UC6 - Exibir Atrasadas

![UC6](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Documentos/CasosDeUso/UC6-EXIBIR_ATRASADAS.png) 