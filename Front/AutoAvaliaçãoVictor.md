# Auto-Avaliação Final


#### Victor Vidal Vaz

Atividades: Fiquei encarregado da parte de prototipação com interação usando mockups e a documentação do usuário, enquanto a codificação e relatório técnico final ficaram sob responsabilidade dos outros integrantes do grupo.

##### [Vídeo da apresentação](https://youtu.be/zJ93Xyx9xZg)

- [x] 14/12 a 18/12: Reuniões com os membros do grupo pra entender a ideia de como as telas deveriam ser compostas, captura de opiniões e inicialização da prototipação utilizando o AdobeXD;

- [x] 21/12 a 26/12: Disponibilização da tela Home ao desenvolvedor com aprovação do grupo;

- [x] 28/12 a 03/01: Desenvolvimento do protótipo e disponibilização da segunda tela ao desenvolvedor;

- [x] 11/01 a 16/01: Compreensão do código e de como inicializar a aplicação para compor o manual do usuário;

- [x] 17/01 a 22/01: Criação do manual, commit das atividades realizadas e gravação do vídeo explicativo.
- [Commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/4d3b199517477b3c9c476fa006e89e90b7728e5f) referente a criação do manual do usuário;
- [Commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/f84cf3f22878723f1d386175910713a09789e95c) do mockup das duas telas principais;
- [Commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/53af9d517f70ffa07968b8070368720cc95263d1) do link de interação entre as telas;

