# Auto-Avaliação Final


#### Vinicius Barreto Costa

Atividades: Fiquei encarregado da parte de dar continuidade a construção do relatório técnico.

##### [Vídeo da apresentação](https://youtu.be/y47GSdjTeNE)

- [x] 14/12 a 18/12: Reuniões com os membros do grupo pra entender a ideia de como as telas deveriam ser compostas.

- [x] 21/12 a 26/12: Analise da tela Home junto ao desenvolvedor com aprovação do grupo;

- [x] 28/12 a 03/01: Analise do protótipo da segunda tela ao desenvolvedor;

- [x] 11/01 a 16/01: Compreensão e entendimento do código do front-end para atuar na criação do relatório técnico.

- [x] 17/01 a 22/01: Criação e desenvolvimento do complemento do relatório técnico.

- [Commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/b001266e005e7888018e0f439d041d3d3529a116) Iniciando relatório técnico;
- [Commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/4b1e07c790c22483e6f2d4886e77e58c0dba6a37) Aplicando correções no relatório técnico;
- [Commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/012b136e6f8f1c3fb7b31111f0b4de666ae7e1b6) Finalizando relatório técnico;

