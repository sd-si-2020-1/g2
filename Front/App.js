import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';

import Task from './src/views/Task';
import Home from './src/views/Home';

const Routes = createAppContainer(
  createSwitchNavigator({
    Home,
    Task
  })
);

console.disableYellowBox = true;
export default function App() {
  return <Routes/>;
}
