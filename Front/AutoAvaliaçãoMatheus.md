# Auto-Avaliação Final


#### Matheus Henrique de Brito

Atividades: Fiquei encarregado da parte de codificação total do código, enquanto a documentação, relatório técnico final e mockups ficaram sob responsabilidade dos outros integrantes do grupo.

##### [Vídeo da apresentação](https://www.youtube.com/watch?v=dhVdc-GyBW4)

- [x] 14/12 a 18/12: Entendimento das ferramentas e estudo sobre React, React Native, Expo, Android Studio (Apesar de não ter sido utilizado devido a minha máquina não suportar tanto, gastei um tempinho tentando utilizá-lo como emulador) e XAMPP

- [x] 21/12 a 26/12: Ínicio da codificação da parte Front-End.
- Projeto iniciado, neste momento foi desenvolvido apenas a página inicial da aplicação, com todos os seus componentes, a qual é usada para mostrar as tarefas do usuário. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/33ec30c6649aa2d75ee11d1948604a30aa830a02)


- [x] 28/12 a 03/01: Conexão com a API:
- Neste passo foi realizado a conexão para alimentar a página principal feita no passo anterior com as tarefas cadastradas no banco de dados MongoDB e acessadas pela API online. Foi utilizado para isso o Axios e o XAMPP para criar um servidor. [commit](https://gitlab.com/sd-si-2020-1/g2/-/commit/0896e2400f57c55ac4a3e26bff1a1ecf91ba5f5d)


- [x] 04/01 a 11/01: Segunda Tela:
- Criação da segunda e última tela da aplicação e finalização da interação que gera a navegação entre as duas telas criadas e também os ícones. [commit1](https://gitlab.com/sd-si-2020-1/g2/-/commit/5650da9f94786b09afd474926051076004cf4cae)  [commit2](https://gitlab.com/sd-si-2020-1/g2/-/commit/50c69213e403f37c7bae40edc491def70ee2dc5e) 


- [x] 11/01 a 16/01: Finalização:
- Tentativa de correção de alguns bugs e finalização com mudanças em algumas características de estilização para ficar adequado aos modelos de mockups desenvolvidos pelo grupo [commit1](https://gitlab.com/sd-si-2020-1/g2/-/commit/db4d112d5e97028f71f9e955114935cdb7a268d1)  [commit2](https://gitlab.com/sd-si-2020-1/g2/-/commit/bb263ba35aaf0b4241426d40642e15a782bf79e8) 


- [x] 17/01 a 22/01: Apoio ao Grupo:
- Nesta etapa me dediquei a dar suporte no que for necessário em questões de dúvidas e fornecimento de informações sobre o funcionamento do código aos outros dois integrantes do grupo.

