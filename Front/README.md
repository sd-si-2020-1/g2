# Sistema Gerenciador de Tarefas - SGT

O SGT é um projeto que visa facilitar o cotidiano das pessoas, desenvolvido na disciplina de Aplicações Distribuídas sob a orientação do professor [Marcelo Akira Inuzuka](http://lattes.cnpq.br/0701953063536783)

### Sumário

* [Prototipação](https://gitlab.com/sd-si-2020-1/g2/-/edit/teste/Front/README.md#prototipa%C3%A7%C3%A3o)
* [Manual do usuário](https://gitlab.com/sd-si-2020-1/g2/-/edit/teste/Front/README.md#manual-do-usu%C3%A1rio)
* [Relatório Técnico](https://gitlab.com/sd-si-2020-1/g2/-/edit/teste/Front/README.md#relat%C3%B3rio-t%C3%A9cnico)

### Prototipação
Para acessar ao protótipo interativo do SGT - Sistema Gerenciador de Tarefas e navegar com um clique nos locais possíveis vendo como o sistema reage e ilustrando suas funcionalidades superficialmente clique [AQUI!](https://xd.adobe.com/view/b2ecda3b-a9ba-44c5-aaba-ada3acd0c174-e7bf/?fullscreen)


### Manual do usuário

Para ter acesso ao Frontend do SGT é necessário baixar e instalar os seguintes softwares:

 (Clique no nome para realizar os downloads)

* [Nodejs](https://nodejs.org/en/)

* [Visual Studio Code](https://code.visualstudio.com/download)

* [Expo](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=pt_BR&gl=US)

* [XAMPP](https://www.apachefriends.org/pt_br/download.html) 

  

O XAMPP será utilizado para emular um servidor de rede Apache permitindo a conexão entre a API e o Celular.

Para executar corretamente o XAMPP é necessário permitir a regra Apache HTTP no firewall do windows, para isso siga as seguinte etapas:

1. Abra o painel de controle através da barra de pesquisa do Windows ou usando Winkey + R e digitando "Control";
2. Vá na opção "Sistema e Segurança";
3. Selecione a opção "Windows Defender Firewall";
4. Configurações Avançadas;
5. Regras de Entrada;
6. Habilite as regras com nome "Apache HTTP Server";

![Firewall](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Front/Documenta%C3%A7%C3%A3o/Imagens/Firewall.png)

Após feito, inicialize o XAMMP e vá frente ao serviço Apache clicando na ação "Start" para que seja possível a conexão.

![XAMMP](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Front/Documenta%C3%A7%C3%A3o/Imagens/XAMMP.png)

Abra o VSCode, dentro dele um novo terminal e execute os comandos a seguir (linha a linha, um por vez) para baixar o repositório do SGT e executá-lo.

```
git clone https://gitlab.com/sd-si-2020-1/g2.git
cd \GerenciadordeTarefa\ 
nodemon \src\index.js 

```
![Backend](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Front/Documenta%C3%A7%C3%A3o/Imagens/Backend.png)

Para executar o Frontend abra uma nova janela do VSCode com um novo terminal e insira o comando a seguir:

`expo start`

![Frontend](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Front/Documenta%C3%A7%C3%A3o/Imagens/Frontend.png)

Ao finalizar a execução, o expo abrirá uma página de internet contendo um QRCode para leitura. 

![Expo0](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Front/Documenta%C3%A7%C3%A3o/Imagens/Expo0.png)

Esse QRCode deve ser lido com expo instalado no Celular, para que o mesmo abra o projeto e possa ser acompanhada todas as alterações no projeto ou interações com o aplicativo quando pronto.

![Expo1](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Front/Documenta%C3%A7%C3%A3o/Imagens/Expo1.png)

Realizada leitura, é possível visualizar a tela gerada anteriormente no Mockup, e realizar interações.

![Expo2](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Front/Documenta%C3%A7%C3%A3o/Imagens/Expo2.png)

# Relatório Técnico.

### Resumo

Com a chegada da pandemia e a migração de várias equipes de trabalho para o home-office vimos a necessidade de ter um controle de suas atividades na palma da mão facilitando o dia-a-dia da combinação entre tarefas diárias da casa e o trabalho. Para organização dos horários abraçamos a idéia da criação de um aplicativo para gerenciamento de tarefas onde se pode inserir, acompanhar e gerenciar toda sua agenda direto do seu celular. Nessa segunda parte do projeto vamos mostrar um pouco do Frontend desenvolvido para o nosso aplicativo.

### Mockups

Como etapa preparatória para o desenvolvimento do front-end, foram criados mockups, de acordo com as especificações de requisitos do software e o código já desenvolvido no back-end.

Selecionamos do mockup algumas telas que são representativas das principais funcionalidades da aplicação.

Na Figura 1, podemos ver a página principal do aplicativo a tela home contendo algumas informações de exemplo de uso.

Na figura 2 podemos ver a tela de navegação dentro de uma tarefa selecionada para que o usuário tenha funcionalidades dentro da mesma.

![homeFinal](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Front/Documentação/Imagens/homeFinal.PNG)

Figura 1. Tela Home.

![TelaNavegacao](https://gitlab.com/sd-si-2020-1/g2/-/raw/teste/Front/Documentação/Imagens/TelaNavegacao.PNG)

Figura 2. Navegação dentro da tarefa.

### Front-end

Assim que o desenvolvimento do back-end foi concluído e as funções básicas do aplicativo foram esboçadas por meio do modelo, o front-end do aplicativo foi desenvolvido com base no trabalho concluído nas etapas anteriores. 

O front-end é construído usando React Native, permitindo que o aplicativo funcione em Android e iOS. A biblioteca axios é usada para se comunicar com o back-end para solicitar API na porta programada na fase de pré-projeto.

### Conclusão

No front end, prioriza-se a realização de algumas melhorias na acessibilidade do aplicativo, bem como a implementação de outros aspectos que podem ser investigados por meio de testes de usabilidade, ou seja, pesquisas sobre a forma como o usuário interage com o aplicativo e quais pontos podem ser apontados, otimizando elementos existentes ou adicione novos elementos.

Podemos acreditar que nossa aplicação atingiu satisfatoriamente o produto mínimo viável e podemos continuar a desenvolver o produto até que ele atinja o nível de maturidade exigido pelo ambiente de produção.
