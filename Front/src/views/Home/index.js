import React, {useState, useEffect} from 'react';

import { View, Text, TouchableOpacity, ScrollView, ActivityIndicator} from 'react-native';

import styles from './styles';

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import TaskCard from '../../components/TaskCard';
import * as Network from 'expo-network';


import api from '../../services/api';


export default function Home({navigation}){
    const [filter, setFilter] = useState('today');
    const [tasks, setTasks] = useState([]);
    const [load, setLoad] = useState(false);
    const [lateCount, setLateCount] = useState();
    const [macaddress, setMacaddress] = useState();

    async function getMacAddress(){
        await Network.getMacAddressAsync().then(mac => {
            setMacaddress(mac);
        });
    }


    async function lateVerify(){
        await api.get(`/task/filter/${filter}/${macaddress}`).then(response => {
            setTasks(response.data)
            setLateCount(response.data.length)
        });
    }

    async function loadTasks(){
        setLoad(true);
        await api.get(`/task/filter/${filter}/${macaddress}`).then(response => {
            setTasks(response.data)
            setLoad(false);
        });
    }

    function Notification(){
        setFilter('late');
    }

    function New(){
        navigation.navigate('Task');
    }

    function Show(id){
        navigation.navigate('Task', {idtask: id});
    }

    useEffect(() => {
        getMacAddress().then(()=> {
            loadTasks();
        });
        lateVerify();
    }, [filter]);


    return (
    <View style={styles.container}>
        <Header showNotification = {true} showBack={false} pressNotification={Notification} late={lateCount} />
        <View style = {styles.filter}>
            <TouchableOpacity onPress={() =>  setFilter('all')}>
                <Text style= { filter == 'all' ? styles.filterTextActived : styles.filtertextInative}>
                    Todos
                </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() =>  setFilter('today')}>
                <Text style= { filter == 'today' ? styles.filterTextActived : styles.filtertextInative}>
                    Hoje
                </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() =>  setFilter('month')}>
                <Text style= { filter == 'month' ? styles.filterTextActived : styles.filtertextInative}>
                    Mês
                </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() =>  setFilter('week')}>
                <Text style= { filter == 'week' ? styles.filterTextActived : styles.filtertextInative}>
                    Semana
                </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() =>  setFilter('year')}>
                <Text style= { filter == 'year' ? styles.filterTextActived : styles.filtertextInative}>
                    Ano
                </Text>
            </TouchableOpacity>
        </View>


        <ScrollView style = {styles.content} contentContainerStyle = {{alignItems: 'center'}}>
            {
                load 
                ?
                <ActivityIndicator color='#000000' size= {50} />
                : 
                tasks.map(t => 
                (
                    <TaskCard done={t.done} title={t.title} when={t.when} type= {t.type} onPress={() => Show(t._id)}/>
                ))
            }
        </ScrollView>

        <Footer icon = {'add'} onPress={New} />
    </View>
    )
}