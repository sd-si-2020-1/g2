import React, {useState, useEffect } from 'react';

import {
    View, 
    ScrollView,
    Image,
    Text,
    TextInput,
    KeyboardAvoidingView,
    TouchableOpacity,
    Switch,
    Alert,
    ActivityIndicator
} from 'react-native';

import * as Network from 'expo-network';


import api from '../../services/api';

import styles from './styles';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import typeIcons from '../../utils/typeIcons';
import DateTimeInput from '../../components/DateTimeInput/index.android';

export default function Task({navigation}){
    const [done, setDone] = useState(false);
    const [type, setType] = useState();
    const [title, setTitle] = useState();
    const [description, setDescription] = useState();
    const [date, setDate] = useState();
    const [hour, setHour] = useState();
    const [macaddress, setMacaddress] = useState();
    const [id, setId] = useState(false);
    const [load, setLoad] = useState(true);

    async function getMacAddress(){
        await Network.getMacAddressAsync().then(mac => {
            setMacaddress(mac);
            setLoad(false);
        });
    }

    async function Remove() {
        Alert.alert(
            'Remover tarefa',
            'Deseja remover?',
            [
                {text: 'Cancelar'},
                {text: 'Confirmar', onPress: () => DeleteTask},
            ],
            {cancelable: true}
        )
    }

    async function DeleteTask() {
        await api.delete(`/task/${id}`).then(() =>{
            navigation.navigate('Home');
        });
    }
    
    useEffect(() => {
        getMacAddress();
        if(navigation.state.params){
            setId(navigation.state.params.idTask)
            LoadTask().then(() => setLoad(false));
        }
    });
    
    async function SaveTask(){
        Alert.alert()

        if(!title)
        return Alert.alert('Defina o nome da tarefa!')

        if(!description)
        return Alert.alert('Defina a descrição')

        if(!type)
        return Alert.alert('Defina o tipo')

        if(!date)
        return Alert.alert('Defina a data')

        if(!hour)
        return Alert.alert('Defina a hora')
        if(id){
            await api.put(`/task/${id}`, {
                macaddress,
                done,
                type,
                title,
                description,
                when: `${date}T${hour}.000`
            }).then(() => {
                navigation.navigate('Home');
            });
        }else{

        await api.post('/task',{
            macaddress,
            type,
            title,
            description,
            when: `${date}T${hour}.000`

        }).then(() => {
            navigation.navigate('Home');
        })
        }
    }

    async function LoadTask(){
        await api.get(`task/${id}`).then(response => {
            setLoad(true);
            setType(response.data.type);
            setDone(response.data.done);
            setTitle(response.data.type);
            setDescription(response.data.description);
            setDate(response.data.when);
            setHour(response.data.when);
        })
    }

    return (
        <KeyboardAvoidingView behavior='padding' style={styles.container}>
            <Header showBack={true} navigation={navigation}/>
            
            {
                load
                ?
            <ActivityIndicator color='#000000' size= {50} />
                :
            <ScrollView style = {{width: '100%'}}>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{marginVertical: 10}}>
                {

                typeIcons.map((icon, index) => (
                <TouchableOpacity onPress={() => setType(index)}>
                    <Image source={icon} style={[styles.imageIcon, type && type != index && styles.typeIconInative]}/>
                </TouchableOpacity>
                ))
            }
            </ScrollView>
            <Text style={styles.label}>Título</Text>
            <TextInput
                style={styles.input}
                maxLength={30}
                placeholder="Lembre-me de fazer..."
                onChangeText={(text) => setTitle(text)}
                value={title}
            />
            <Text style={styles.input}>Detalhes</Text>
            <TextInput
                style={styles.inputarea}
                maxLength={200}
                multiline={true}
                placeholder="Detalhes da atividade que eu tenho que lembrar ..."
                onChangeText={(text) => setDescription(text)}
                value={description}
/>

            <DateTimeInput type={'hour'} save={setHour} date={date}/>
            <DateTimeInput type={'date'} save={setDate} hour={hour}/>

            {
                id &&
            <View style={styles.inLine}>
                <View style={styles.inputInline}>
                    <Switch onValueChange = {() => setDone(!done)} value = {done} thumbColor={done ? '#00761b' : '#000000'}/>
                    <Text style={styles.switchLabel}>Concluído</Text>
                </View>
            <TouchableOpacity onPress={Remove}>
                <Text style={styles.removeLabel}>Excluir</Text>
            </TouchableOpacity>
            </View>
            }
        </ScrollView>
}
        <Footer icon={'save'} onPress={SaveTask}/>
        </KeyboardAvoidingView>
    )
}