import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        alignItems: 'flex-start',
        justifyContent:'flex-start'
    },
    imageIcon: {
        width: 40,
        height: 40,
        marginHorizontal: 10
    },
    label: {
        color: '#707070',
        fontSize: 18,
        paddingHorizontal: 10,
        marginTop: 20,
        marginBottom: 5
    },
    input:{
        fontSize: 18,
        padding: 10,
        width: '90%',
        borderBottomWidth: 1,
        borderBottomColor: '#EE6B26',
        marginHorizontal: 10
    },

    inputarea:{
        fontSize: 18,
        padding: 10,
        width: '90%',
        borderBottomWidth: 1,
        borderBottomColor: '#EE6B26',
        marginHorizontal: 10,
        borderRadius: 10,
        height: 100,
        textAlignVertical: 'top'
    },

    inputInline: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10
    },

    switchLabel: {
        fontWeight: 'bold',
        color: '#EE6b26',
        textTransform: 'uppercase',
        fontSize: 16,
        paddingLeft: 10
    },
    removeLabel: {
        fontWeight: 'bold',
        color: '#000000',
        textTransform: 'uppercase',
        fontSize: 16
    },
    inLine: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 10
    },
    typeIconInative: {
        opacity: 0.5
    }



});

export default styles;
