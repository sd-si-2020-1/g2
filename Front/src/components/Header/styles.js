import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 70,
        backgroundColor: '#000000',
        borderWidth: 10,
        borderColor: '#EE6B26',
        alignItems: 'center',
        justifyContent: 'center'
    },

    logo: {
        width: 100,
        height: 30
    },

    notification: {
        position: 'absolute', 
        right: 20          
    },

    notificationImage: {
        width: 20,
        height: 25
    },

    circle: {
        width: 20,
        backgroundColor: '#FFF',
        borderRadius: 50,
        alignItems: 'center',
        position: 'absolute',
        left: 13,
        bottom: 13


    },

    leftIcon: {
        position: 'absolute',
        left: 20
    }



});

export default styles;
