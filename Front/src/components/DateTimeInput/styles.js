import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    input:{
        fontSize: 18,
        padding: 10,
        width: '95%',
        borderBottomColor: '#EE6B26',
        borderBottomWidth: 1,
        marginHorizontal: 10
    },

    iconTextInput:{
        position: 'absolute',
        left: '95%',
        top: 15,
        width: 25,
        height: 25,
        resizeMode: 'contain'
    }
});

export default styles;